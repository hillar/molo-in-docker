FROM ubuntu:18.04

MAINTAINER 'no maintenance intended'
 
RUN env

RUN apt-get -qq update && \
apt-get -qq update && \
apt-get install -yq tshark && \
apt-get install -yq  wget curl libpcre3-dev uuid-dev libmagic-dev pkg-config g++ flex bison zlib1g-dev libffi-dev gettext libgeoip-dev make libjson-perl libbz2-dev libwww-perl libpng-dev xz-utils libffi-dev python libssl-dev libyaml-dev ethtool && \
rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Declare args
# https://files.molo.ch/builds/ubuntu-18.04/moloch_1.8.0-1_amd64.deb
# ARG MOLOCH_VERSION=ubuntu18_amd64
# ARG UBUNTU_VERSION=18.04
ARG MOLOCHDIR=/data/moloch
ARG SPOOLDIR=$MOLOCHDIR/spool
ARG ES_HOST=elasticsearch
ARG ES_PORT=9200
ARG MOLOCH_PASSWORD=PASSWORD
ARG MOLOCH_INTERFACE=eth0
ARG CAPTURE=off
ARG VIEWER=on
ARG MONITOR=off
ARG INITALIZEDB=false
ARG WIPEDB=false

# Declare envs vars for each arg
ENV ES_HOST $ES_HOST
ENV ES_PORT $ES_PORT
ENV MOLOCH_LOCALELASTICSEARCH no
ENV MOLOCH_ELASTICSEARCH "http://"$ES_HOST":"$ES_PORT
ENV MOLOCH_INTERFACE $MOLOCH_INTERFACE
ENV MOLOCH_PASSWORD $MOLOCH_PASSWORD
ENV MOLOCHDIR $MOLOCHDIR
ENV SPOOLDIR $SPOOLDIR
ENV CAPTURE $CAPTURE
ENV VIEWER $VIEWER
ENV MONITOR $MONITOR
ENV INITALIZEDB $INITALIZEDB
ENV WIPEDB $WIPEDB


# https://files.molo.ch/builds/ubuntu-18.04/moloch_1.8.0-1_amd64.deb
# RUN cd /tmp && curl -C - -O "https://files.molo.ch/builds/ubuntu-"$UBUNTU_VERSION"/moloch_"$MOLOCH_VERSION".deb"
# RUN cd /tmp && dpkg -i "moloch_"$MOLOCH_VERSION".deb"

# https://files.molo.ch/moloch-master_ubuntu18_amd64.deb
RUN cd /tmp && curl -s -C - -O "https://files.molo.ch/moloch-master_ubuntu18_amd64.deb" && dpkg -i moloch-master_ubuntu18_amd64.deb
#RUN cd $MOLOCHDIR/etc && curl -s -C - -O "https://raw.githubusercontent.com/aol/moloch/master/release/config.ini.sample"

RUN echo $SPOOLDIR
RUN echo $MOLOCHDIR
RUN [ -d $(dirname $MOLOCHDIR) ] || mkdir $(dirname $MOLOCHDIR)
RUN [ -d $MOLOCHDIR ] || ln -s /data/moloch $MOLOCHDIR
RUN mkdir -p $SPOOLDIR
RUN cd $MOLOCHDIR/etc && pwd && ls -tlah

# add scripts
ADD /scripts $MOLOCHDIR/scripts/
# add config.ini 
#ADD /etc /data/moloch/etc/
RUN chmod 755 $MOLOCHDIR/scripts/*.sh

#Update Path
ENV PATH=${MOLOCHDIR}/scripts:${MOLOCHDIR}/bin:${PATH}

EXPOSE 8005
WORKDIR $MOLOCHDIR

ENTRYPOINT ["entrypoint.sh"]
