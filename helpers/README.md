## auto convert and load new files from spool

1. [watcher](helpers/watch-pcaps-spool.bash ) watches for new files and calls converter with *$dir* and *$filename*
1. [converter](helpers/convert-snoop.bash) converts and moves *$filename* from spool to archive, but before checks for dir do exists in archvie, if not calls capture launcher with *$filename*
1. [capture launcher](helpers/new-dir-from-convert.bash) tells docker container to start capture and monitor dir of *$filename* with **tags** set to every **subdir name**

----

watcher is [started with systemd](helpers/watch-pcap-spool.service) 

![](helpers/Screenshot_2019-05-30_at_09.21.03.png)
