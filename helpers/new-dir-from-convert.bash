#!/bin/bash                                                                                                                           

dir=$(dirname $1)                                                                                                                     
tags=$(echo -en $dir| sed 's/\.//'|sed 's/\// --tag=/g')                                                                              
log=$(echo -en $dir| sed 's/\.//'|sed 's/\//_/g')                                                                                     
echo "$(date) $1 $dir $tags" >> /var/log/pcaps-newdir.log                                                                             
molo=$(docker ps|grep moloch|cut -f1 -d" ")                                                                                           

docker exec $molo moloch-capture -c /opt/moloch/etc/config.ini --pcapdir /opt/moloch/spool/${dir} --monitor --copy ${tags}       
